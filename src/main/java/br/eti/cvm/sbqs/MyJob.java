package br.eti.cvm.sbqs;

import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class MyJob implements Job {

	@Autowired
	private Scheduler scheduler;

	@Bean
	public void mySheduling() throws SchedulerException {
		final JobDetail jobDetail = JobBuilder
				.newJob(this.getClass())
				.withIdentity(this.getClass().getSimpleName())
				.build();

		final Trigger trigger = TriggerBuilder
				.newTrigger()
				.forJob(jobDetail)
				.withSchedule(SimpleScheduleBuilder
						.simpleSchedule()
						.withIntervalInSeconds(10)
						.repeatForever())
				.startNow()
				.build();

		scheduler.scheduleJob(jobDetail, trigger);
		scheduler.start();
	}

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		log.info("Executed each 10 seconds...");
	}

}
